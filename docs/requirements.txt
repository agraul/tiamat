sphinx==3.1.2
sphinx-material==0.0.30
Sphinx-Substitution-Extensions==2020.5.30.0
sphinx-copybutton==0.2.11
sphinx-notfound-page==0.4
