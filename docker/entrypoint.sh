#!/bin/bash -i

# Fail on errors.
set -e

# Make sure .bashrc is sourced
. /root/.bashrc

# Allow the workdir to be set using an env var.
# Useful for CI pipiles which use docker for their build steps
# and don't allow that much flexibility to mount volumes
WORKDIR=${SRCDIR:-/src}
TIAMAT_LOG_LEVEL=${TIAMAT_LOG_LEVEL:-debug}

cd $WORKDIR

# Add any extra CLI args to the tiamat build command
CMD="tiamat --log-level=$TIAMAT_LOG_LEVEL build --directory=$WORKDIR"
for ARG in "$@"; do
  CMD+=" ${ARG}"
done

# Run the tiamat build command
echo "${CMD}"
TIAMAT_BUILD_PACKAGE=$($CMD) || { echo 'tiamat build failed' ; exit $?; }

# Statically link the binary if this was called from staticx
if [ "$USE_STATIC_X" = true ]; then
  staticx --strip --loglevel=DEBUG $TIAMAT_BUILD_PACKAGE $TIAMAT_BUILD_PACKAGE
  strip $TIAMAT_BUILD_PACKAGE
fi

# Change the owner of the binary from root to the user that ran the docker command
chown -R --reference=. ./dist/

# Try to use the resulting binary while we are still inside the container
env -i $TIAMAT_BUILD_PACKAGE --help > /dev/null || { echo 'Failed to execute binary' ; exit $?; }
