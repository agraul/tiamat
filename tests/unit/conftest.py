# Import python libs
import pytest
from dict_tools import data


@pytest.fixture(scope="function")
def hub(hub):
    hub.pop.sub.add(dyne_name="config")
    hub.pop.sub.add(dyne_name="tiamat")
    yield hub


@pytest.fixture(scope="function")
def bname(hub):
    """
    Initialize the build structure
    """
    build_name = "test_build"
    hub.tiamat.BUILDS[build_name] = data.NamespaceDict(build=data.NamespaceDict())
    yield build_name
