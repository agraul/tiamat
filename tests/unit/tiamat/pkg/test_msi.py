from unittest import mock


def test_build(hub, mock_hub, bname):
    mock_hub.tiamat.pkg.msi.build = hub.tiamat.pkg.msi.build
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].name = "test_name"
    mock_hub.tiamat.BUILDS[bname].pkg = {"candle_path": ""}

    mock_hub.tiamat.cmd.run.return_value = mock.MagicMock(
        retcode=0, stdout="", stderr=""
    )

    mock_run = mock.MagicMock()
    mock_run.returncode = 0
    with mock.patch("os.mkdir"):
        mock_hub.tiamat.pkg.msi.build(bname)
