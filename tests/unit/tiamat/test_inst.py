from unittest import mock


def test_mk_spec(hub, mock_hub, bname):
    mock_hub.tiamat.inst.mk_spec = hub.tiamat.inst.mk_spec
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].venv_dir = "test_venv_dir"
    mock_hub.tiamat.BUILDS[bname].s_path = "test_s_path"
    mock_hub.tiamat.BUILDS[bname].name = "test_name"
    mock_hub.tiamat.BUILDS[bname].imports = []
    mock_hub.tiamat.BUILDS[bname].datas = []
    mock_hub.tiamat.BUILDS[bname].pypi_args = "test args"
    mock_hub.tiamat.BUILDS[bname].spec = "test.spec"
    mock_hub.tiamat.BUILDS[bname].cmd = ["pyinstaller"]

    spec = "spec data"
    with mock.patch("builtins.open", mock.mock_open(read_data=spec)):
        mock_hub.tiamat.inst.mk_spec(bname)

    assert mock_hub.tiamat.BUILDS[bname].cmd == ["pyinstaller", "test.spec"]


def test_call(hub, mock_hub, bname):
    mock_hub.tiamat.inst.call = hub.tiamat.inst.call
    mock_hub.tiamat.cmd.run.return_value = mock.MagicMock(retcode=0)

    with mock.patch("os.makedirs"), mock.patch("shutil.copy") as sc:
        mock_hub.tiamat.inst.call(bname)
        sc.assert_called_once()
