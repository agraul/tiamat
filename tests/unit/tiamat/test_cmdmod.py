import subprocess
from unittest import mock


def test_run(hub, mock_hub):
    mock_hub.tiamat.cmd.run = hub.tiamat.cmd.run

    proc = mock.MagicMock()
    proc.communicate.return_value = (b"stdout", b"stderr")
    cmd = ["command", "args"]
    timeout = 999

    with mock.patch("subprocess.Popen", return_value=proc) as mock_popen:
        ret = mock_hub.tiamat.cmd.run(cmd, timeout=timeout)

        # Verify dictionary keys in output
        assert "retcode" in ret
        assert "stdout" in ret
        assert "stderr" in ret
        assert "pid" in ret

        # Verify attributes in output
        assert hasattr(ret, "retcode")
        assert hasattr(ret, "stdout")
        assert hasattr(ret, "stderr")
        assert hasattr(ret, "pid")

        # Verify that Popen was called
        mock_popen.assert_called_once_with(
            cmd, shell=False, stdout=subprocess.PIPE, stderr=subprocess.PIPE
        )

    proc.communicate.assert_called_once_with(timeout=timeout)
    proc.wait.assert_called_once_with(timeout=timeout)
