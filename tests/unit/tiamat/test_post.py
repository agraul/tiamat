from unittest import mock


def test_report(hub, mock_hub, bname):
    mock_hub.tiamat.post.report = hub.tiamat.post.report
    with mock.patch("builtins.print") as mock_print:
        mock_hub.tiamat.post.report(bname)
        mock_print.assert_called_once()


def test_clean(hub, mock_hub, bname):
    mock_hub.tiamat.post.clean = hub.tiamat.post.clean
    with mock.patch("shutil.rmtree") as sr, mock.patch("os.remove") as osr:
        mock_hub.tiamat.post.clean(bname)
        sr.assert_called_once()
        osr.assert_called()
