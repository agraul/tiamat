from unittest import mock


def test_env(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.venv.env = hub.tiamat.virtualenv.venv.env
    mock_hub.tiamat.virtualenv.venv.env(bname)


def test_bin(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.venv.bin = hub.tiamat.virtualenv.venv.bin
    mock_hub.tiamat.virtualenv.venv.bin(bname)


def test_create(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.venv.create = hub.tiamat.virtualenv.venv.create
    mock_hub.tiamat.virtualenv.venv.create(bname)
