from unittest import mock


def test_bin(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.pyenv.bin = hub.tiamat.virtualenv.pyenv.bin
    mock_hub.tiamat.virtualenv.pyenv.bin(bname)


def test_create(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.pyenv.create = hub.tiamat.virtualenv.pyenv.create
    mock_hub.tiamat.virtualenv.pyenv.create(bname)
