from unittest import mock

from dict_tools import data


def test_bin(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.init.bin = hub.tiamat.virtualenv.init.bin
    mock_hub.OPT = data.NamespaceDict(tiamat=data.NamespaceDict(venv_plugin="system"))

    mock_hub.tiamat.virtualenv.init.bin(bname)
    mock_hub.tiamat.virtualenv.system.bin.assert_called_once_with(bname)


def test_freeze(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.init.freeze = hub.tiamat.virtualenv.init.freeze
    mods = {
        "idem-grains @ file://home/tiamat/idem-grains",
        "pop==14",
        "pytest-pop==6.3",
    }
    mock_hub.tiamat.cmd.run.return_value = data.NamespaceDict(stdout="\n".join(mods))
    freeze = mock_hub.tiamat.virtualenv.init.freeze(bname)
    assert freeze == mods


def test_freeze_exclude(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.init.freeze = hub.tiamat.virtualenv.init.freeze
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].pybin = ""
    mock_hub.tiamat.BUILDS[bname].srcdir = ""
    mock_hub.tiamat.BUILDS[bname].exclude = {
        "pop-config",
        "idem-grains",
        "pop==13",
    }
    mods = {
        "idem-grains @ file://home/tiamat/idem-grains",
        "pop==14",
        "pytest-pop==6.3",
    }
    mock_hub.tiamat.cmd.run.return_value = data.NamespaceDict(stdout="\n".join(mods))
    freeze = mock_hub.tiamat.virtualenv.init.freeze(bname)
    assert freeze == {"pytest-pop==6.3"}


def test_create(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.init.create = hub.tiamat.virtualenv.init.create
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].venv_plugin = "pyenv"

    mock_hub.tiamat.virtualenv.init.create(bname)
    mock_hub.tiamat.virtualenv.pyenv.create.assert_called_once_with(bname)


def test_env(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.init.env = hub.tiamat.virtualenv.init.env
    ret = mock_hub.tiamat.virtualenv.init.env(bname)
    assert ret == ["env", "PYTHONUTF8=1", "LANG=POSIX"]


def test_scan(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.init.scan = hub.tiamat.virtualenv.init.scan
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].vroot = "test_vroot"
    mock_hub.tiamat.BUILDS[bname].omit = ["d"]
    mock_hub.tiamat.BUILDS[bname].all_paths = set()

    with mock.patch("os.walk", return_value=[("a", "b", "c"), ("d", None, None)]):
        mock_hub.tiamat.virtualenv.init.scan(bname)

    assert mock_hub.tiamat.BUILDS[bname].all_paths == {"a/c", "a/b"}


def test_mk_adds(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.init.mk_adds = hub.tiamat.virtualenv.init.mk_adds
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].imports = set()
    mock_hub.tiamat.BUILDS[bname].all_paths = {"site-packages/foo.py"}
    mock_hub.tiamat.BUILDS[bname].datas = set()

    with mock.patch("os.path.isfile", return_value=True), mock.patch(
        "os.path.isdir", return_value=True
    ):
        mock_hub.tiamat.virtualenv.init.mk_adds(bname)

    assert mock_hub.tiamat.BUILDS[bname].datas == {"site-packages/foo.py:foo.py"}
    assert mock_hub.tiamat.BUILDS[bname].imports == {"foo"}


def test_setup_pip(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.init.setup_pip = hub.tiamat.virtualenv.init.setup_pip
    mock_hub.tiamat.BUILDS = hub.tiamat.BUILDS
    mock_hub.tiamat.BUILDS[bname].srcdir = "test_src_dir"
    mock_hub.tiamat.BUILDS[bname].dir = "test_dir"
    mock_hub.tiamat.BUILDS[bname].dev_pyinst = True
    mock_hub.tiamat.BUILDS[bname].system_copy_in = False
    mock_hub.tiamat.BUILDS[bname].exclude = None
    mock_hub.tiamat.BUILDS[bname].venv_dir = "/dev/null"
    mock_hub.tiamat.BUILDS[bname].pybin = "/dev/null/python"
    mock_hub.tiamat.cmd.run.return_value = mock.MagicMock(stdout="(3, 6, 4)")

    with mock.patch("os.listdir", return_value=["foo.whl"]), mock.patch(
        "builtins.open"
    ):
        mock_hub.tiamat.virtualenv.init.setup_pip(bname)
        mock_hub.tiamat.cmd.run.assert_called_with(
            [
                "/dev/null/python",
                "-m",
                "pip",
                "install",
                "foo.whl",
                "https://github.com/pyinstaller/pyinstaller/tarball/develop",
            ],
            cwd="test_src_dir",
            fail_on_error=True,
        )
