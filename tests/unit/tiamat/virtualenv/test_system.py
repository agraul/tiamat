from unittest import mock


def test_bin(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.system.bin = hub.tiamat.virtualenv.system.bin
    mock_hub.tiamat.virtualenv.system.bin(bname)


def test_create(hub, mock_hub, bname):
    mock_hub.tiamat.virtualenv.system.create = hub.tiamat.virtualenv.system.create
    with mock.patch("os.symlink"), mock.patch("os.makedirs") as mock_mkdrs:
        mock_hub.tiamat.virtualenv.system.create(bname)
        mock_mkdrs.assert_called_once()
